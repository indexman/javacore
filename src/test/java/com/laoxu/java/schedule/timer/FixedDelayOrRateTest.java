package com.laoxu.java.schedule.timer;

import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @Description:
 * @Author laoxu
 * @Date 2021/6/19 15:01
 **/
public class FixedDelayOrRateTest {
    @Test
    void fixedDelayTest() throws InterruptedException {
         Timer timer = new Timer("Timer");
         TimerTask task = new TimerTask() {
            @Override
            public void run() {
                System.out.println("Start: scheduleAtFixedDelay:    " + new Date());
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("End  : scheduleAtFixedDelay:    " + new Date());
            }
        };
        timer.schedule(task,1000,2000);

        Thread.sleep(1000L * 30);
    }

    @Test
    void fixedRateTest() throws InterruptedException {
         Timer timer = new Timer("Timer");
         TimerTask task = new TimerTask() {
            @Override
            public void run() {
                System.out.println("Start: scheduleAtFixedRate:    " + new Date());
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("End  : scheduleAtFixedRate:    " + new Date());
            }
        };
        timer.scheduleAtFixedRate(task, 1000L, 2000L);

        Thread.sleep(1000L * 30);
    }
}
