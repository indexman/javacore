package com.laoxu.java.collection;

import java.util.Enumeration;
import java.util.Vector;

/**
 * 测试枚举接口，
 * 可用于遍历集合类型，目前已被迭代器Iterator取代
 *
 * @author laoxu
 * @create 2017-12-25
 **/
public class EnumerationTest {
    public static void main(String[] args) {
        Vector v = new Vector();
        v.add("Jack");
        v.add("ate");
        v.add("lots of oranges.");
        Enumeration<String> e = v.elements();
        String output = "";
        while (e.hasMoreElements()) {
            output += e.nextElement() + " ";
        }

        System.out.println(output);
    }
}
