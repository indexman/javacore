package com.laoxu.java.collection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author laoxu
 * @create 2018-01-21
 **/
public class HashMapTest {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "H");
        map.put(2, "e");
        map.put(3, "l");
        map.put(4, "l");
        map.put(5, "o");

        //iterator遍历
        Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();
        String str = "";
        while (it.hasNext()) {

            str += it.next().getValue();
        }

        //输出Hello
        System.out.println(str);
    }
}
