package com.laoxu.java.collection;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Arrays;

/**
 * @author laoxu
 * @create 2018-01-21
 **/
public class HashSetTest {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("123");
        set.add("123");//覆盖
        set.add("456");
        set.add("abc");
        set.add("efg");

        //Iterator遍历
        Iterator it = set.iterator();
        while (it.hasNext()){
            System.out.println(it.next());
        }

        //foreach遍历，前提是指定了元素类型为String
        for(String str:set){
            System.out.println();
        }
    }
}
