package com.laoxu.java.collection;

import java.util.HashSet;
import java.util.Iterator;

/**
 * @author laoxu
 * @create 2018-01-27
 **/
public class IterateHashSet {
    public static void main(String[] args) {
        // Create a HashSet
        HashSet<String> hset = new HashSet<String>();

        //add elements to HashSet
        hset.add("Chaitanya");
        hset.add("Rahul");
        hset.add("Tim");
        hset.add("Rick");
        hset.add("Harry");
        //1.use Iterator
        System.out.println("----use Iterator:");
        Iterator<String> it = hset.iterator();
        while(it.hasNext()){
            System.out.println(it.next());
        }

        //2.use loop
        System.out.println("----use loop:");
        for (String temp : hset) {
            System.out.println(temp);
        }
    }
}
