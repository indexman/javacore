package com.laoxu.java.collection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author laoxu
 * @create 2018-01-21
 **/
public class ArrayListTest {
    public static void main(String[] args) {
        List<String> list=new ArrayList<>();
        list.add("W");
        list.add("o");
        list.add("r");
        list.add("l");
        list.add("d");

        Iterator<String> it = list.iterator();
        String str="";
        while (it.hasNext()){
            str+=it.next();
        }

        //输出World
        System.out.println(str);
    }
}
