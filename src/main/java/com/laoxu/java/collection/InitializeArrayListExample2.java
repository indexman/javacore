package com.laoxu.java.collection;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author laoxu
 * @create 2018-01-28
 **/
public class InitializeArrayListExample2 {
    public static void main(String args[]) {
        ArrayList<String> cities = new ArrayList<String>(){{
            add("Delhi");
            add("Agra");
            add("Chennai");
        }};
        System.out.println("Content of Array list cities:"+cities);
    }
}
