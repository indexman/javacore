package com.laoxu.java.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;

/**
 * @author laoxu
 * @create 2018-01-28
 **/
public class LoopArrayListExample {
    public static void main(String[] args) {

        ArrayList<String> arrayList = new ArrayList<String>();
        arrayList.add("Jack");
        arrayList.add("Tom");
        arrayList.add("Lucy");
        arrayList.add("Brant");

      /* For Loop for iterating ArrayList */
        System.out.println("For Loop");
        for (int counter = 0; counter < arrayList.size(); counter++) {
            System.out.println(arrayList.get(counter));
        }

      /* Advanced For Loop*/
        System.out.println("Advanced For Loop");
        for (String str : arrayList) {
            System.out.println(str);
        }

      /* While Loop for iterating ArrayList*/
        System.out.println("While Loop");
        int count = 0;
        while (arrayList.size() > count) {
            System.out.println(arrayList.get(count));
            count++;
        }

      /*Looping Array List using Iterator*/
        System.out.println("Iterator");
        Iterator iter = arrayList.iterator();
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }

        /*using Enumeration*/
        // Get the Enumeration object
        Enumeration<String> e = Collections.enumeration(arrayList);

        // Enumerate through the ArrayList elements
        System.out.println("Enumeration: ");
        while(e.hasMoreElements()){
            System.out.println(e.nextElement());
        }
    }



}
