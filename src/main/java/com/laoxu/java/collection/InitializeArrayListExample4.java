package com.laoxu.java.collection;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author laoxu
 * @create 2018-01-28
 **/
public class InitializeArrayListExample4 {
    public static void main(String args[]) {
        ArrayList<Integer> intlist = new ArrayList<Integer>(Collections.nCopies(10, 5));
        System.out.println("ArrayList items: "+intlist);
    }
}
