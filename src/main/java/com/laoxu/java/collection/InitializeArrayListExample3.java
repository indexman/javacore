package com.laoxu.java.collection;

import java.util.ArrayList;

/**
 * @author laoxu
 * @create 2018-01-28
 **/
public class InitializeArrayListExample3 {
    public static void main(String args[]) {
        ArrayList<String> books = new ArrayList<String>();
        books.add("Java Book1");
        books.add("Java Book2");
        books.add("Java Book3");
        System.out.println("Books stored in array list are: "+books);
    }
}
