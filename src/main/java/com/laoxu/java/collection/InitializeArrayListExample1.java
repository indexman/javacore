package com.laoxu.java.collection;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author laoxu
 * @create 2018-01-28
 **/
public class InitializeArrayListExample1 {
    public static void main(String args[]) {
        ArrayList<String> obj = new ArrayList<String>(
                Arrays.asList("Pratap", "Peter", "Harsh"));
        System.out.println("Elements are:"+obj);
    }
}
