package com.laoxu.java.multithread;

/**
 *  请求中断一个线程，目标线程通过检测isInterrupted()标志获取自身是否已中断。如果目标线程处于等待状态，该线程会捕获到InterruptedException
 */
public class InterruptThreadExample {
    public static void main(String[] args) throws InterruptedException {
        // 创建线程
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!Thread.currentThread().isInterrupted()) {
                    System.out.println("线程正在运行...");
                    try {
                        // 模拟线程执行任务
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        // 当线程被中断时，需要重新设置中断标志位
                        Thread.currentThread().interrupt();
                    }
                }
                System.out.println("线程被中断，结束运行。");
            }
        });

        // 启动线程
        thread.start();

        // 主线程休眠3秒后，中断子线程
        Thread.sleep(3000);
        thread.interrupt();
    }
}
