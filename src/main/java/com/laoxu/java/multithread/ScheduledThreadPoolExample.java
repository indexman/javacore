package com.laoxu.java.multithread;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 */
public class ScheduledThreadPoolExample {
    public static void main(String[] args) throws InterruptedException {
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(2);

        // 定义一个Runnable任务
        Runnable task = () -> System.out.println("执行任务：" + System.currentTimeMillis());

        // 使用FixedRate模式，每隔1秒触发一次任务
        scheduledExecutorService.scheduleAtFixedRate(task, 0, 1, TimeUnit.SECONDS);

        // 使用FixedDelay模式，每次任务执行完毕后，等待1秒再执行下一次任务
        scheduledExecutorService.scheduleWithFixedDelay(task, 0, 1, TimeUnit.SECONDS);

        // 让主线程等待一段时间，以便观察任务执行情况
        Thread.sleep(5000);

        // 关闭调度器
        scheduledExecutorService.shutdown();
    }
}
