package com.laoxu.java.multithread;

/***
 *
 *  可以把ThreadLocal看成一个全局Map<Thread, Object>：每个线程获取ThreadLocal变量时，总是使用Thread自身作为key：
 *  ThreadLocal相当于给每个线程都开辟了一个独立的存储空间，各个线程的ThreadLocal关联的实例互不干扰。
 *
 */
public class ThreadLocalExample {
    // 创建一个ThreadLocal变量
    private static final ThreadLocal<Integer> threadLocal = new ThreadLocal<>();

    public static void main(String[] args) {
        // 设置线程局部变量的值
        setValue(10);

        // 获取线程局部变量的值
        System.out.println("当前线程的值为：" + getValue());

        // 创建一个新的线程并设置其线程局部变量的值
        Thread newThread = new Thread(() -> {
            setValue(20);
            System.out.println("新线程的值为：" + getValue());
        });

        // 启动新线程
        newThread.start();

        // 获取主线程的线程局部变量的值
        System.out.println("主线程的值为：" + getValue());
    }

    // 设置线程局部变量的值的方法
    public static void setValue(int value) {
        threadLocal.set(value);
    }

    // 获取线程局部变量的值的方法
    public static int getValue() {
        return threadLocal.get();
    }
}
