package com.laoxu.java.multithread;

/**
 *  守护线程（Daemon Thread）是一种在后台运行的线程，它的主要作用是为其他线程提供服务。
 *  当所有非守护线程结束时，守护线程会自动结束。守护线程不能阻止程序的退出，它们主要用于:
 *  1.日志记录：在程序运行期间定时写入日志文件。
 *  2.垃圾回收：定时进行垃圾回收，以防止内存泄漏。
 *
 * 监控任务：监控程序运行状态，以检测异常情况。
 *
 * 后台任务：执行一些后台任务，不影响程序的正常运行。
 */
public class DaemonThreadExample {
    public static void main(String[] args) {
        // 创建一个守护线程
        Thread daemonThread = new Thread(() -> {
            while (true) {
                try {
                    System.out.println("守护线程正在运行...");
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        // 将线程设置为守护线程
        daemonThread.setDaemon(true);

        // 启动守护线程
        daemonThread.start();

        try {
            // 主线程休眠5秒
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("主线程结束");
    }
}
