package com.laoxu.java.multithread;

/**
 *  volatile关键字在Java中用于修饰变量，表示该变量可能会被多个线程同时访问，因此volatile关键字在Java中用于修饰变量，表示该变量可能会被多个线程同时访问，
 *  因此需要确保每次读取该变量时都能获取到最新的值。当一个线程修改了一个volatile变量的值，其他线程可以立即看到这个修改
 */
public class VolatileExample {
    private volatile int count = 0; // 使用volatile关键字修饰count变量

    public void increment() {
        count++; // 对count变量进行自增操作
    }

    public int getCount() {
        return count; // 返回count变量的值
    }

    public static void main(String[] args) throws InterruptedException {
        VolatileExample example = new VolatileExample();

        // 创建两个线程，分别对count变量进行自增操作
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                example.increment();
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                example.increment();
            }
        });

        t1.start();
        t2.start();

        // 等待两个线程执行完毕
        t1.join();
        t2.join();

        // 输出count变量的值，由于使用了volatile关键字，所以结果应该是2000
        System.out.println("count: " + example.getCount());
    }
}
