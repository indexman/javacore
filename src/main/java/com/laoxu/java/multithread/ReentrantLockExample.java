package com.laoxu.java.multithread;

import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockExample {
    private final ReentrantLock lock = new ReentrantLock();

    public void doSomething() {
        lock.lock(); // 获取锁
        try {
            // 在这里执行需要同步的代码
            System.out.println("开始执行任务");
            Thread.sleep(1000);
            System.out.println("任务执行完毕");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock(); // 释放锁
            System.out.println("释放锁");
        }
    }

    public static void main(String[] args) {
        ReentrantLockExample example = new ReentrantLockExample();
        example.doSomething();
    }
}
