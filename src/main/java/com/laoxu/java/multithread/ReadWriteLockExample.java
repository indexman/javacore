package com.laoxu.java.multithread;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 *  适用条件是同一个数据，有大量线程读取，但仅有少数线程修改
 *  只允许一个线程写入（其他线程既不能写入也不能读取）；
 *  没有写入时，多个线程允许同时读（提高性能）。
 *  读的过程中不允许写，这是一种悲观的读锁。
 *
 * 使用ReadWriteLock来保护一个共享变量count。当有多个线程尝试读取count时，它们都可以同时获得读锁，而只有一个线程可以获得写锁。这样可以提高读取操作的性能
 */
public class ReadWriteLockExample {
    private int count = 0;
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    public void increment() {
        readWriteLock.writeLock().lock();
        try {
            count++;
            System.out.println("当前计数：" + count);
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    public void decrement() {
        readWriteLock.writeLock().lock();
        try {
            count--;
            System.out.println("当前计数：" + count);
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    public void read() {
        readWriteLock.readLock().lock();
        try {
            System.out.println("当前计数：" + count);
        } finally {
            readWriteLock.readLock().unlock();
        }
    }

    public static void main(String[] args) {
        ReadWriteLockExample example = new ReadWriteLockExample();

        // 创建10个读线程和2个写线程
        for (int i = 0; i < 10; i++) {
            new Thread(example::read).start();
        }
        for (int i = 0; i < 2; i++) {
            new Thread(example::increment).start();
            new Thread(example::decrement).start();
        }
    }
}
