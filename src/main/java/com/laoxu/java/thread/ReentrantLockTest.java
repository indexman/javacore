package com.laoxu.java.thread;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author laoxu
 * @Date 2022/11/23 11:42
 * @Desc ReentrantLockTest
 */
public class ReentrantLockTest implements Runnable{
    int ticketNums = 10;

    private final ReentrantLock lock = new ReentrantLock();

    @Override
    public void run() {
        while (true){
            try{
                lock.lock();
                if(ticketNums > 0){
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(ticketNums--);
                }else {
                    break;
                }
            }finally {
                lock.unlock();
            }
        }
    }

    public static void main(String[] args) {
        ReentrantLockTest  rt = new ReentrantLockTest();
        new Thread(rt,"t1").start();
        new Thread(rt,"t2").start();
        new Thread(rt,"t3").start();
    }
}
