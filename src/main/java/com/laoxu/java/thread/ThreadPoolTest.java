package com.laoxu.java.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author laoxu
 * @Date 2022/11/23 15:26
 * @Desc 线程池
 */
public class ThreadPoolTest {
    public static void main(String[] args) {
        // 创建执行器服务，定义一个线程池
        ExecutorService service = Executors.newFixedThreadPool(10);
        service.execute(new MyThread());
        service.execute(new MyThread());
        service.execute(new MyThread());
        // 关闭
        service.shutdown();
    }

}

class MyThread implements Runnable{
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
    }
}
