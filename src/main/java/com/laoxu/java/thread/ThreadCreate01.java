package com.laoxu.java.thread;

/**
 * @Author laoxu
 * @Date 2022/11/22 14:21
 * @Desc 创建线程方式1：继承Thread类
 */
public class ThreadCreate01 extends Thread{
    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println("I'm coding---"+i);
        }
    }

    public static void main(String[] args) {
        ThreadCreate01 t1 = new ThreadCreate01();
        t1.start();
        //t1.run();
        for (int i = 0; i < 20; i++) {
            System.out.println("我在写多线程代码+++"+i);
        }
    }
}
