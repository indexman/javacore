package com.laoxu.java.thread.ch2.c01.task;

/**
 * This class simulates a bank or a cash dispenser that takes money
 * from an account
 * 
 */
public class Bank implements Runnable {

	/**
	 * The account affected by the operations
	 */
	private Account account;
	
	/**
	 * Constructor of the class. Initializes the account
	 * @param account The account affected by the operations
	 */
	public Bank(Account account) {
		this.account=account;
	}
	
	/**
	 * Core method of the Runnable
	 */
	public void run() {
		for (int i=0; i<10; i++){
			account.subtractAmount(1000);
			System.out.println("withdraw 1000, balance:"+account.getBalance());
		}
	}

}
