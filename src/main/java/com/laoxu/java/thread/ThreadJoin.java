package com.laoxu.java.thread;

/**
 * @Author laoxu
 * @Date 2022/11/23 0:05
 * @Desc 线程插队
 */
public class ThreadJoin implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("VIP来了"+i);
        }
    }

    public static void main(String[] args) throws InterruptedException {
       ThreadJoin tj = new ThreadJoin();
       Thread t1 = new Thread(tj);
       t1.start();

        for (int i = 0; i < 500; i++) {
            if(i==200){
                t1.join();
            }
            System.out.println("main"+i);
        }
    }
}
