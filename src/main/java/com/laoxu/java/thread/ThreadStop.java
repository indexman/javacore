package com.laoxu.java.thread;

/**
 * @Author laoxu
 * @Date 2022/11/22 23:37
 * @Desc 线程停止，使用标志位
 */
public class ThreadStop implements Runnable {
    // 设置flag
    private boolean flag = true;

    @Override
    public void run() {
        int i = 0;
        while (flag){
            System.out.println("run..."+i++);
        }
    }

    public void stop(){
        this.flag = false;
    }

    public static void main(String[] args) {
        ThreadStop t1 = new ThreadStop();
        new Thread(t1).start();

        for (int i = 0; i < 1000; i++) {
            System.out.println("main"+i);
            if(i == 900){
                t1.stop();
                System.out.println("run线程停止了");
            }
        }
    }
}
