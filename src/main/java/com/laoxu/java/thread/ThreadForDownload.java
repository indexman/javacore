package com.laoxu.java.thread;

import com.laoxu.java.util.WebDownloader;

/**
 * @Author laoxu
 * @Date 2022/11/22 14:44
 * @Desc 多线程下载图片演示
 */
public class ThreadForDownload extends Thread{
    private String url;  // 图片地址
    private String filename;  // 文件名

    public ThreadForDownload(String url,String filename){
        this.url = url;
        this.filename = filename;
    }

    @Override
    public void run() {
        WebDownloader.download(url,filename);
        System.out.println("下载的文件名为："+filename);
    }

    public static void main(String[] args) {
        ThreadForDownload tfd1 = new ThreadForDownload("http://rlhnyso6f.sabkt.gdipper.com/waimai/product/1583592181575.png","1.png");
        ThreadForDownload tfd2 = new ThreadForDownload("http://rlhnyso6f.sabkt.gdipper.com/waimai/product/1583592181575.png","2.png");
        ThreadForDownload tfd3 = new ThreadForDownload("http://rlhnyso6f.sabkt.gdipper.com/waimai/product/1583592181575.png","3.png");
        ThreadForDownload tfd4 = new ThreadForDownload("http://rlhnyso6f.sabkt.gdipper.com/waimai/product/1583592181575.png","4.png");
        tfd1.start();
        tfd2.start();
        tfd3.start();
        tfd4.start();
    }
}
