package com.laoxu.java.thread;

/**
 * @Author laoxu
 * @Date 2022/11/22 15:03
 * @Desc 创建线程方式1：实现Runnable接口
 */
public class RunnableCreateThread implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println("I'm coding---"+i);
        }
    }

    public static void main(String[] args) {
        RunnableCreateThread rt = new RunnableCreateThread();
        Thread t1 = new Thread(rt);
        t1.start();

        for (int i = 0; i < 20; i++) {
            System.out.println("我在写多线程代码+++"+i);
        }
    }
}
