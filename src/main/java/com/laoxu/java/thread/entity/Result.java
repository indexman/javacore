package com.laoxu.java.thread.entity;

/**
 * @author laoxu
 * @create 2018-04-14
 **/
public class Result {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
