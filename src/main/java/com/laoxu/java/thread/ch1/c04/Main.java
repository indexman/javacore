package com.laoxu.java.thread.ch1.c04;

import java.util.concurrent.TimeUnit;

/**
 * @author laoxu
 * @create 2018-04-23
 **/
public class Main {
    public static void main(String[] args) {
        FileSearch searcher=new FileSearch("C:\\","test.log");
        Thread thread=new Thread(searcher);
        thread.start();
        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        thread.interrupt();
    }
}
