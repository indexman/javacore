package com.laoxu.java.thread.ch1.c03;

/**
 * @author laoxu
 * @create 2018-04-22
 **/
public class PrimeGenerator extends Thread {
    @Override
    public void run() {
        long number = 1L;
        while (true){
            if(isPrime(number)){
                System.out.printf("Number %d is Prime.\n", number);
            }
            //判断是否被打断
            if(isInterrupted()){
                System.out.printf("The Prime Generator has been Interrupted");
                return;
            }
            number++;
        }
    }

    private boolean isPrime(long number){
        if(number<2){
            return true;
        }
        for (long i=2;i<number;i++){
            if((number%i)==0){
                return false;
            }
        }

        return true;
    }


}
