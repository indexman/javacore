package com.laoxu.java.thread.ch1.c06;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author laoxu
 * @create 2018-04-25
 **/
public class NetworkConnectionsLoader implements Runnable {
    @Override
    public void run() {
        // Writes a message
        System.out.printf("Begining network connections loading: %s\n",new Date());
        // Sleep six seconds
        try {
            TimeUnit.SECONDS.sleep(6);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Writes a message
        System.out.printf("Network connections loading has finished: %s\n",new Date());
    }
}
