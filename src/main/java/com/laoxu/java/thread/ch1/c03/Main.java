package com.laoxu.java.thread.ch1.c03;

/**
 * @author laoxu
 * @create 2018-04-22
 **/
public class Main {
    public static void main(String[] args) {
        Thread task = new PrimeGenerator();
        task.start();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        task.interrupt();
    }
}
