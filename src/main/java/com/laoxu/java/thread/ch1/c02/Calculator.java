package com.laoxu.java.thread.ch1.c02;

/**
 * This class prints the multiplication table of a number
 * @author laoxu
 * @create 2018-04-21
 **/
public class Calculator implements Runnable{
    private int number;
    public Calculator(int number) {
        this.number=number;
    }
    @Override
    public void run() {
        for (int i=1; i<=10; i++){
            System.out.printf("%s: %d * %d = %d\n",Thread.
                    currentThread().getName(),number,i,i*number);
        }
    }


}
