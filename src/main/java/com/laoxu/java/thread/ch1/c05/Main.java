package com.laoxu.java.thread.ch1.c05;

import java.util.concurrent.TimeUnit;

/**
 * @author laoxu
 * @create 2018-04-24
 **/
public class Main {
    public static void main(String[] args) {
        FileClock clock=new FileClock();
        Thread thread=new Thread(clock);
        thread.start();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread.interrupt();
    }
}
