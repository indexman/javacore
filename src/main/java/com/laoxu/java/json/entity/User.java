package com.laoxu.java.json.entity;

/**
 * 用户实体
 *
 * @author laoxu
 * @create 2018-01-20
 **/
public class User {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
