package com.laoxu.java.json.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * 部门实体
 *
 * @author laoxu
 * @create 2018-01-20
 **/
public class Organization {
    private int id;
    private String name;
    private List<User> userList = new ArrayList<User>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
