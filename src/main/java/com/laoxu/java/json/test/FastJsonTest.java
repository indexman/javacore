package com.laoxu.java.json.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.laoxu.java.json.entity.Organization;
import com.laoxu.java.json.entity.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 测试阿里巴巴fastjson
 *
 * @author laoxu
 * @create 2018-01-20
 *
 * //下面是FastJson的简介：常用的方法！
 * //  Fastjson API入口类是com.alibaba.fastjson.JSON，常用的序列化操作都可以在JSON类上的静态方法直接完成。
 * //  public static final Object parse(String text); // 把JSON文本parse为JSONObject或者JSONArray
 * //  public static final JSONObject parseObject(String text)； // 把JSON文本parse成JSONObject
 * //  public static final <T> T parseObject(String text, Class<T> clazz); // 把JSON文本parse为JavaBean
 * //  public static final JSONArray parseArray(String text); // 把JSON文本parse成JSONArray
 * //  public static final <T> List<T> parseArray(String text, Class<T> clazz); //把JSON文本parse成JavaBean集合
 * //  public static final String toJSONString(Object object); // 将JavaBean序列化为JSON文本
 * //  public static final String toJSONString(Object object, boolean prettyFormat); // 将JavaBean序列化为带格式的JSON文本
 * //  public static final Object toJSON(Object javaObject); 将JavaBean转换为JSONObject或者JSONArray（和上面方法的区别是返回值是不一样的）
 **/
public class FastJsonTest {
    public static void main(String[] args) {
        //json字符串转换为JSONObject
        jsonToJSONObject();
        //json字符串转换为bean
        jsonToBean();
        //bean转json字符串
        beanToJson();
        //map转换为json字符串
        mapToJson();
        //json字符串转JSONArray
        jsonToJSONArray();
    }

    private static void beanToJson() {
        System.out.println("------------------beanToJson------------------");
        User user = new User();
        user.setId(1);
        user.setName("钱多多");
        String json = JSON.toJSONString(user,true);
        System.out.println(json);
    }

    private static void jsonToJSONArray() {
        System.out.println("------------------jsonToJSONArray------------------");
        String json="{users:[{\"id\":1,\"name\":\"Dylan\"},{\"id\":2,\"name\":\"Jack\"}]}";
        JSONObject jo = JSON.parseObject(json);
        String jsonArray = jo.get("users").toString();
        System.out.println(jsonArray);
        List<User> userList = JSON.parseArray(jsonArray,User.class);
        for (User user:userList){
            System.out.println(user.getId()+":"+user.getName());
        }
    }

    //以下是静态方法
    private static void jsonToJSONObject() {
        System.out.println("------------------jsonToJSONObject-----------------");
        String json = "{\"id\":1,\"name\":\"Dylan\"}";
        JSONObject jsonObject = JSON.parseObject(json);
        System.out.println(jsonObject.getInteger("id")+":"+jsonObject.getString("name"));
    }

    private static void jsonToBean(){
        System.out.println("------------------jsonToBean-------------------------");
        String json = "{\"id\":1,\"name\":\"dylan\"}";
        User user = JSON.parseObject(json,User.class);
        System.out.println(user.getId()+":"+user.getName());
    }

    private static void mapToJson(){
        System.out.println("-------------------mapToJson--------------------------");
        Organization organization = new Organization();
        organization.setId(1);
        organization.setName("研发部");
        User user1 = new User();
        user1.setId(1);
        user1.setName("钱多多");
        User user2 = new User();
        user2.setId(2);
        user2.setName("穷不怕");
        organization.getUserList().add(user1);
        organization.getUserList().add(user2);

        Map<String,Object> orgMap = new HashMap<String,Object>();
        orgMap.put("orgId",organization.getId());
        orgMap.put("orgName",organization.getName());
        orgMap.put("userList",organization.getUserList());

        String json = JSON.toJSONString(orgMap);
        System.out.println(json);
    }


}
