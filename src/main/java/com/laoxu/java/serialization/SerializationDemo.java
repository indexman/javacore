package com.laoxu.java.serialization;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 序列化例子
 *
 * @author laoxu
 * @create 2018-01-08
 **/
public class SerializationDemo {
    public static void main(String[] args) throws IOException{
        //学生1
        Student s1 = new Student(101,"Jack","Male");
        //学生2
        Student s2 = new Student(102,"Lily","Female");

        List<Student> list = new ArrayList<Student>();
        list.add(s1);
        list.add(s2);

        //创建文件流
        FileOutputStream fos = new FileOutputStream("D:\\test\\student01.ser");
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(list);

        os.close();
        System.out.println("序列化完成！");
    }
}
