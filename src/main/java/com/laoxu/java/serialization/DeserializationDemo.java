package com.laoxu.java.serialization;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 反序列化例子
 *
 * @author laoxu
 * @create 2018-01-08
 **/
public class DeserializationDemo {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("D:\\test\\student01.ser");
        ObjectInputStream is = new ObjectInputStream(fis);
        Object obj = null;
        List<Student> list = new ArrayList<Student>();
        try {
            list = (List<Student>)is.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        is.close();

        //遍历list，输出
        for (Student student:list){
            System.out.println(student.toString());
        }
    }
}
