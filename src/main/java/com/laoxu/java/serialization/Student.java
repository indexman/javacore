package com.laoxu.java.serialization;

import java.io.Serializable;

/**
 * 学生类
 *
 * @author laoxu
 * @create 2018-01-08
 **/
public class Student implements Serializable {

    private static final long serialVersionUID = -2110227637642817458L;
    private static String mess="null";

    private int id;
    private String name;
    private String gender;
    private int age;
    private transient String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Student(int id, String name, String gender) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        mess = "something";
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", password='" + password + '\'' +
                ", mess='" + mess + '\'' +
                '}';
    }
}
