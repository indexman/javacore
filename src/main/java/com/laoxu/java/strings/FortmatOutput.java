package com.laoxu.java.strings;

/**
 * @Description:
 * @Author laoxu
 * @Date 2020/5/13 22:08
 **/
public class FortmatOutput {
    public static void main(String[] args) {
        int x = 5;
        double y = 5.332542;

        // old way
        System.out.println("Row 1:["+x+","+y+"]");
        // new way
        System.out.printf("Row 1:[%d,%f]",x,y);
    }
}
