package com.laoxu.java.strings;

import java.util.Arrays;

/**
 * @Description:
 * @Author laoxu
 * @Date 2020/5/14 9:20
 **/
public class SplitDemo {
    public static String content="As the last ship sailed towards the decent horizon...I sat there watching you at rock, " +
            "my mind slowly drift away, forming into my dream tale.";

    public static void split(String regex){
        System.out.println(Arrays.toString(content.split(regex)));
    }

    public static void main(String[] args) {
        split(" "); // 以空格分隔
        split("\\W+"); // 不是字母的都换成","
        split("n\\W+"); // 把n结尾的后面不是字母的换成了","
    }
}
