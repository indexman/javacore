package com.laoxu.java.schedule.timer;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * @Description:
 * @Author laoxu
 * @Date 2021/6/19 16:06
 **/
public class ScheduleTest {
    static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public static void main(String[] args) {
        scheduler.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                System.out.println("Start: scheduleWithFixedDelay: " + new Date());
                try {
                    Thread.sleep(12000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("End  : scheduleWithFixedDelay: " + new Date());
            }
        }, 0, 10 , SECONDS);
    }
}
