package com.laoxu.java.newfeatures;

/**
 * @Author laoxu
 * @Date 2022/11/22 22:57
 * @Desc 介绍lambda表达式
 */
public class LambdaDemo {
    // 2.静态内部类
    static class Like2 implements ILike{
        @Override
        public void lambda() {
            System.out.println("I like lambda2.");
        }
    }

    public static void main(String[] args) {
        ILike like = new Like();
        like.lambda();

        like = new Like2();
        like.lambda();
        // 3.局部内部类
        class Like3 implements ILike{
            @Override
            public void lambda() {
                System.out.println("I like lambda3.");
            }
        }
        like = new Like3();
        like.lambda();
        // 4.匿名内部类
        like = new ILike() {
            @Override
            public void lambda() {
                System.out.println("I like lambda4.");
            }
        };
        like.lambda();
        // 5.lambda表达式
        like = ()->{
            System.out.println("I like lambda5.");
        };
        like.lambda();
    }
}

interface ILike{
    void lambda();
}

class Like implements ILike{
    @Override
    public void lambda() {
        System.out.println("I like lambda.");
    }
}
