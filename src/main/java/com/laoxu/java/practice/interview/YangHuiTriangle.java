package com.laoxu.java.practice.interview;

/**
 * 打印杨辉三角
 *
 * 原理
 * 1.每个数等于它上方两数之和
 * 2.第n行的数字有n个
 *
 * @author laoxu
 * @create 2018-03-06
 **/
public class YangHuiTriangle {
    public static void main(String[] args) {
        print(10);
    }

    public static void print(int n) {
        int[][] a = new int[n][n];

        for (int i = 0; i < n; i++) {
            //格式化输出
            System.out.format("%" + (n - i) * 2 + "s", "");
            for (int j = 0; j <= i; j++) {
                if (j == 0 || j == i) {
                    a[i][j] = 1;
                } else {
                    a[i][j] = a[i - 1][j - 1] + a[i - 1][j];
                }

                System.out.format("%4d", a[i][j]);
            }

            System.out.println();
        }
    }
}
