package com.laoxu.java.practice.interview;

import java.util.Scanner;

/**
 * 判断闰年
 *
 * @author laoxu
 * @create 2018-02-27
 **/
public class LeapYearTest {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String input = "";
        int year;
        System.out.println("请输入一个年份：");
        while (true) {
            input = sc.next();
            if("q".equals(input.toLowerCase())){
                break;
            }
            try {
                year = Integer.parseInt(input);
                if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
                    System.out.println(year + "是闰年！");
                } else {
                    System.out.println(year + "不是闰年！");
                }
            } catch (NumberFormatException e) {
                System.out.println("请输入一个正整数！");
            }
        }

        System.out.println("程序运行结束！");

    }
}
