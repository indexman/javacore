package com.laoxu.java.practice.interview;

/**
 * 打印九九乘法表
 *
 * @author laoxu
 * @create 2018-03-07
 **/
public class Multiply99 {
    public static void main(String[] args) {
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j+"*"+i+"="+i*j+"\t");
            }

            System.out.println();
        }
    }
}
