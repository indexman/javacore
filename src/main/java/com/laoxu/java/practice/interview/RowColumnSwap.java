package com.laoxu.java.practice.interview;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 互换二维数组的行列
 * <p>
 * 假设存在二维数组：
 * {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}
 * 交换前：
 * 1 2 3
 * 4 5 6
 * 7 8 9
 * <p>
 * 交换后：
 * 1 4 7
 * 2 5 8
 * 3 6 9
 *
 * @author laoxu
 * @create 2018-03-08
 **/
public class RowColumnSwap {
    public static void main(String[] args) {
        int[][] arr = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        System.out.println("before swap:" );
        printArray(arr);

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = arr[j][i];
            }
        }
        System.out.println("after swap:" );
        printArray(arr);
    }

    //打印二维数组
    public static void printArray(int[][] arr){
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j]+"\t");
            }
            System.out.println();
        }
    }


}
