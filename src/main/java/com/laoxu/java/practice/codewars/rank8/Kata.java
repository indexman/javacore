package com.laoxu.java.practice.codewars.rank8;

import static java.lang.Math.abs;

/**
 * 返回负数
 *
 * @author laoxu
 * @create 2017-11-22
 **/
public class Kata {
    public static void main(String[] args) {
        System.out.println(bonusTime(25000, true));
    }


    public static String bonusTime(final int salary, final boolean bonus) {
        // show me the code!
        //return bonus?("£"+salary*10):"£"+salary;
        return "\u00A3" + (bonus ? 10 : 1) * salary;

    }



    /**
     * 猜拳游戏
     * @param p1
     * @param p2
     * @return
     */
    public static String rps(String p1, String p2) {
        if(p1==p2){
            return "Draw!";
        }

        if("scissors".equals(p1)){
            if("paper".equals(p2)){
                return "Player 1 won!";
            }else{
                return "Player 2 won!";
            }
        }

        if("rock".equals(p1)){
            if("scissors".equals(p2)){
                return "Player 1 won!";
            }else{
                return "Player 2 won!";
            }
        }

        if("paper".equals(p1)){
            if("rock".equals(p2)){
                return "Player 1 won!";
            }else{
                return "Player 2 won!";
            }
        }

        return "Who win?";
    }

    public static String rps2(String p1, String p2) {
        if(p1 == p2) return "Draw!";
        int p = (p1 + p2).equals("scissorspaper") || (p1 + p2).equals("rockscissors") || (p1 + p2).equals("paperrock") ? 1 : 2;
        return "Player " + p + " won!";
    }

    public static String stringy(int size) {
        if(size<=0){
            return "";
        }else{
            StringBuffer sb=new StringBuffer();
            for (int i = 0; i < size; i++) {
                if(i%2==0){
                    sb.append("1");
                }else {
                    sb.append("0");
                }
            }

            return sb.toString();
        }
    }

    public static int makeNegative(final int x) {
        //return x <= 0 ? x : (0 - x);
        return -abs(x);
    }

    public static String numberToString(int num) {
        return String.valueOf(num);
    }

    public static double find_average(int[] array) {
        //数组求和
        int sum = 0;
        int len = array.length;

        if (len == 0) {
            return 0;
        } else {
            for (int i = 0; i < len; i++) {
                sum += array[i];
            }

            return (double) sum / len;
        }
    }
}
