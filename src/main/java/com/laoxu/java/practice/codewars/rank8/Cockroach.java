package com.laoxu.java.practice.codewars.rank8;

/**
 * 蟑螂的速度
 *
 * @author laoxu
 * @create 2017-11-26
 **/
public class Cockroach {
    public int cockroachSpeed(double x){
        return (int)Math.floor(100000 * x / 3600);
    }
}
