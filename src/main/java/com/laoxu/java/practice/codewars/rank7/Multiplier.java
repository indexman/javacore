package com.laoxu.java.practice.codewars.rank7;

/**
 * 2个整数相乘
 *
 * @author laoxu
 * @create 2017-12-04
 **/
public class Multiplier {
    public static int multiply(int a, int b) {
        /*if (a != 0 && b != 0) {
            if ((Integer.MAX_VALUE-1) / Math.abs(a) < Math.abs(b)) {
                throw new ArithmeticException();
            }
        }*/

        return a * b;
    }

    public static void main(String[] args) {
        System.out.println(multiply(0, 0));
        System.out.println(multiply(234, 567));
        System.out.println(Multiplier.multiply(39650, 54161));
        System.out.println(multiply(Integer.MAX_VALUE, Integer.MAX_VALUE));
        System.out.println(multiply(Integer.MAX_VALUE, Integer.MIN_VALUE));
    }
}
