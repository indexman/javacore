package com.laoxu.java.practice.codewars.rank8;

/**
 * 数组元素化整为零
 *
 * @author laoxu
 * @create 2017-11-26
 **/
public class SmashWords {
    public static String smash(String... words) {
        String output="";
        if(words.length==0){
            return "";
        }else{
            for(String word:words){
                output+=word+" ";
            }
        }

        return output.trim();
    }
}
