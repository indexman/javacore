package com.laoxu.java.practice.codewars.rank8;

/**
 * @author laoxu
 * @create 2017-11-28
 **/
public class RepeatIt {
    /**
     * 返回重复N次的字符串
     *
     * @param toRepeat
     * @param n
     * @return
     */
    public static String repeatString(final Object toRepeat, final int n) {
        if (toRepeat instanceof String) {
            String result = "";
            for (int i = 0; i < n; i++) {
                result += toRepeat;
            }

            return result;
        } else {
            return "Not a string";
        }
    }

    public static String repeatString2(final Object toRepeat, final int n) {
        if (!(toRepeat instanceof String)) return "Not a string";
        //用StringBuilder明显更好！
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < n; i++) {
            sb.append(toRepeat);
        }

        return sb.toString();

    }


    public static void main(String[] args) {
        System.out.println(repeatString(0, 1));
        System.out.println(repeatString("Hello", 10));
    }
}
