package com.laoxu.java.practice.codewars.rank7;

/**
 * Growth of a Population
 *
 * @author laoxu
 * @create 2017-12-09
 **/
public class Arge {
    public static int nbYear(int p0, double percent, int aug, int p) {
        int i = 0;

        while (p0 < p) {
            p0 = (int) (p0 * (1 + percent / 100)) + aug;
            i++;
        }

        return i;
    }

    public static void main(String[] args) {

    }
}
