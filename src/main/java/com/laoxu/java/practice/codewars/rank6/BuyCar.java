package com.laoxu.java.practice.codewars.rank6;

import java.util.Arrays;

/**
 * 啥时候能买上新车
 *
 * @author laoxu
 * @create 2017-12-03
 **/
public class BuyCar {

    public static void main(String[] args) {
        //int[] r = new int[] { 6, 766 };
        int[] r1 = BuyCar.nbMonths(2000, 8000, 1000, 1.5);
        int[] r2 = BuyCar.nbMonths(12000, 8000, 1000, 1.5);
        System.out.println(Arrays.toString(r1));
        System.out.println(Arrays.toString(r2));
    }

    public static int[] nbMonths(int startPriceOld, int startPriceNew, int savingperMonth, double percentLossByMonth) {
        // your code

        if(startPriceOld>=startPriceNew){
            return new int[]{0,startPriceOld-startPriceNew};
        }else{
            int[] result = new int[2];
            double saving=startPriceOld,
                    priceOld=startPriceOld,
                    priceNew=startPriceNew,
                    lossRate=percentLossByMonth;
            int numOfMonth=0;
            while (saving<priceNew){
                numOfMonth++;
                if(numOfMonth%2==0){
                    lossRate+=0.5;
                }

                saving = saving+savingperMonth-priceOld*lossRate/100;
                priceOld=priceOld*(1-lossRate/100);
                priceNew=priceNew*(1-lossRate/100);

            }

            result[0]=numOfMonth;
            result[1] =(int)(Math.round(saving - priceNew));

            return result;
        }

    }

}
