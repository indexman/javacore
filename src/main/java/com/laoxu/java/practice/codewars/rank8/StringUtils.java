package com.laoxu.java.practice.codewars.rank8;

import java.util.regex.Pattern;

/**
 * 字符串工具类
 *
 * @author laoxu
 * @create 2017-12-14
 **/
public class StringUtils {
    /**
     * 判断入参是否全为数字
     * @param s
     * @return
     */
    public static boolean isDigit(String s) {
        return Pattern.matches("[0-9]+",s);
    }

    public static void main(String[] args) {
        System.out.println(isDigit(""));
        System.out.println(isDigit("aaa"));
        System.out.println(isDigit("123"));
        System.out.println(isDigit("-123"));
    }
}
