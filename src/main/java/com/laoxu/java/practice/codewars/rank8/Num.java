package com.laoxu.java.practice.codewars.rank8;

/**
 * @author laoxu
 * @create 2017-11-27
 **/
public class Num {
    //返回第N个偶数
    public static int nthEven(int n) {
        return 2 * (n - 1);
    }
}
