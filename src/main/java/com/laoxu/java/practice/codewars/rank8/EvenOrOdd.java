package com.laoxu.java.practice.codewars.rank8;

/**
 * 偶数还是奇数
 *
 * @author laoxu
 * @create 2017-11-25
 **/
public class EvenOrOdd {
    public static String even_or_odd(int number){
        return number%2==0?"Even":"Odd";
    }

}
