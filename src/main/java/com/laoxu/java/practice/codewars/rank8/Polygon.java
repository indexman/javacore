package com.laoxu.java.practice.codewars.rank8;

/**
 * 多边形类型
 *
 * @author laoxu
 * @create 2017-11-26
 **/
public class Polygon {
    int sides;
    int sideLength;

    public Polygon(int sides, int sideLength) {
        this.sides = sides;
        this.sideLength = sideLength;
    }

    public double circleDiameter(){
        //Your code comes here
        return sideLength*Math.tan((sides-2)*Math.toRadians(90)/sides);

        //return sideLength*Math.cos(Math.toRadians(180)/sides);


        //return sideLength*Math.cos(Math.toRadians(180)/sides);
    }

}