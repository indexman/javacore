package com.laoxu.java.practice.codewars.rank8;

/**
 * @author laoxu
 * @create 2017-11-29
 **/
public class NoBoring {
    public static int noBoringZeros(int n) {
        return n == 0 ? 0 : Integer.parseInt(Integer.toString(n).replaceAll("0*$", ""));
    }

    /**
     * 循环地取余数，直到余数不能被10整除
     * @param n
     * @return
     */
    public static int noBoringZeros2(int n) {
        if (n == 0)
            return n;

        while (n % 10 == 0)
            n /= 10;

        return n;
    }

    public static void main(String[] args) {
        System.out.println(noBoringZeros(0));
        System.out.println(noBoringZeros(1450));
        System.out.println(noBoringZeros(960000));
    }
}
