package com.laoxu.java.practice.codewars.rank8;

/**
 * @author laoxu
 * @create 2017-11-27
 **/
public class Bob {
    public static int enough(int cap, int on, int wait){
        //return (cap-on-wait)>=0?0:Math.abs(cap-on-wait);
        return Math.max(0, on + wait - cap);
    }
}
