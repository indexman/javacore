package com.laoxu.java.practice.codewars.rank8;

/**
 * @author laoxu
 * @create 2017-11-27
 **/
public class Clock {
    public static int Past(int h, int m, int s)
    {
        return (3600*h+60*m+s)*1000;
    }
}
