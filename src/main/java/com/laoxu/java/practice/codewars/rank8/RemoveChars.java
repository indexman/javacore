package com.laoxu.java.practice.codewars.rank8;

/**
 * @author laoxu
 * @create 2017-12-14
 **/
public class RemoveChars {
    public static String remove(String str) {

        // your code here
        return str.substring(1,str.length()-1);
    }

    public static void main(String[] args) {
        System.out.println(remove("hello"));
    }
}
