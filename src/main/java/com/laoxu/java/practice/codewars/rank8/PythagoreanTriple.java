package com.laoxu.java.practice.codewars.rank8;

import java.util.Arrays;

/**
 * 毕达哥拉斯三角
 *
 * @author laoxu
 * @create 2017-11-26
 **/
public class PythagoreanTriple {
    public int pythagoreanTriple(int[] triple) {
        //1 如果数组长度不是3，返回0
        if (triple.length != 3) {
            return 0;
        } else {
            //2 数组牲畜U排序
            Arrays.sort(triple);
            //3 根据三角公式判断
            int a = triple[0], b = triple[1], c = triple[2];
            if (Math.pow(a, 2) + Math.pow(b, 2) == Math.pow(c, 2)) {
                return 1;
            }else{
                return 0;
            }
        }

    }
}
