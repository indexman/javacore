package com.laoxu.java.practice.codewars.rank8;

import java.util.Set;
import java.util.TreeSet;

/**
 * 2个字符串合并去重排序
 *
 * @author laoxu
 * @create 2017-11-26
 **/
public class TwoToOne {
    public static String longest(String s1, String s2) {
        // 1 将s1和s2拼接
        String s = s1.concat(s2);
        if (s1.length() == 0) {
            return "";
        } else {
            //2 转数组
            char[] chars = s.toCharArray();
            // 2 将数组元素加入TreeSet（完成了自动去重和排序）
            Set<String> charSet = new TreeSet();
            for (char c : chars) {
                charSet.add(String.valueOf(c));
            }
            // 3 将HashSet转为字符串返回
            String sorted="";
            for(String c:charSet){
                sorted+=c;
            }

            return sorted;
        }

    }


}
