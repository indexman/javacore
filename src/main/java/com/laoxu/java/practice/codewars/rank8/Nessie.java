package com.laoxu.java.practice.codewars.rank8;

import java.util.regex.Pattern;

/**
 * 判断字符串中是否包含某字符串
 *
 * @author laoxu
 * @create 2017-12-14
 **/
public class Nessie {
    public static boolean isLockNessMonster(String s) {
        return Pattern.matches(".*(tree fiddy|3\\.50|three fifty).*", s);
    }

    public static void main(String[] args) {
        System.out.println(isLockNessMonster("tree fiddy123"));
    }
}
