package com.laoxu.java.practice.codewars.rank8;

/**
 * 数羊
 *
 * @author laoxu
 * @create 2017-11-26
 **/
public class Counter {
    public int countSheeps(Boolean[] arrayOfSheeps){
        int count=0;
        //循环数组
        for(int i=0; i<arrayOfSheeps.length; i++){
            if(arrayOfSheeps[i]!=null&&arrayOfSheeps[i]){
                count++;
            }
        }

        return count;
    }
}
