package com.laoxu.java.practice.codewars.rank8;

/**
 * @author laoxu
 * @create 2017-11-28
 **/
public class Hello {
    public static String sayHello(String [] name, String city, String state){
        if(name.length==0){
            return "";
        }else{
            StringBuffer sb = new StringBuffer("Hello,");
            for (int i = 0; i < name.length; i++) {
                sb.append(" "+name[i]);
            }

            sb.append("! Welcome to "+city+", "+state+"!");

            return sb.toString();
        }
    }

    //String.join(" ", name) JDK1.8支持
    /*public String sayHello2(String[] name, String city, String state){
        return String.format("Hello, %s! Welcome to %s, %s!",String.join(" ", name),city,state);
    }*/

    /**
     * 利用String.format更加直观，容易错
     * @param name
     * @param city
     * @param state
     * @return
     */
    public String sayHello3(String [] name, String city, String state){
        String fullName = "";
        for (String nam : name){
            fullName += " " + nam;
        }
        return String.format("Hello,%s! Welcome to %s, %s!", fullName, city, state);
    }

    public static void main(String[] args) {
        System.out.println(sayHello(new String[]{"Tom", "Jack"}, "BeiJing", "LianYunGang"));
    }
}
