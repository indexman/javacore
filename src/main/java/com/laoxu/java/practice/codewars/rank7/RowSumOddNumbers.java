package com.laoxu.java.practice.codewars.rank7;

/**
 * 计算偶数金字塔每行之和
 *
 * @author laoxu
 * @create 2017-11-20
 **/
public class RowSumOddNumbers {
    public static int rowSumOddNumbers(int n) {
        return n * n * n;
    }
}
