package com.laoxu.java.practice.codewars.rank7;

/**
 * Convert Time to String
 * @author laoxu
 * @create 2017-12-04
 **/
public class TimeUtils {
    /**
     * 更好
     * @param timeDiff
     * @return
     */
    public static String convertTime2(int timeDiff) {
        String format =  "%s %s %s %s";
        return String.format(format, timeDiff / 86400, timeDiff % 86400 / 3600, timeDiff % 3600 / 60, timeDiff % 60);
    }

    public static String convertTime(int timeDiff){
        int d = timeDiff/(3600*24);
        int h = (timeDiff-3600*24*d)/3600;
        int m = (timeDiff-3600*24*d-3600*h)/60;
        int s = (timeDiff-3600*24*d-3600*h-60*m);

        return d+" "+h+" "+m+" "+s;
    }

    public static void main(String[] args) {
        System.out.println(TimeUtils.convertTime(0));
        System.out.println(TimeUtils.convertTime(90061));
        System.out.println(TimeUtils.convertTime(-90061));;
    }
}
