package com.laoxu.java.practice.codewars.rank7;

/**
 * @author laoxu
 * @create 2017-12-06
 **/
public class Printer {
    public static String printerError(String s) {
        if(s.length()==0){
            return "";
        }else{
            StringBuffer pattern = new StringBuffer("abcdefghijklm");
            char[] chr = s.toCharArray();
            int errorCount=0;
            for(char c:chr){

                if(pattern.indexOf(String.valueOf(c))<0){
                    errorCount++;
                }
            }
            //return "\""+errorCount+"/"+s.length()+"\"";
            return String.format("%d/%d",errorCount,s.length());
        }
    }

    public static String printerErrorBest(String s) {
        return s.replaceAll("[a-m]", "").length() + "/" + s.length();
    }

    public static void main(String[] args) {
        System.out.println(printerError("aaabbbbhaijjjm"));
    }
}
