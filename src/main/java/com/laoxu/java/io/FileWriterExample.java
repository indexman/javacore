package com.laoxu.java.io;

import java.io.FileWriter;

/**
 * @author laoxu
 * @create 2018-01-07
 **/
public class FileWriterExample {
    public static void main(String args[]){
        try{
            FileWriter fw=new FileWriter("D:\\testout.txt");
            fw.write("Welcome to java.io.");
            fw.close();
        }catch(Exception e){System.out.println(e);}
        System.out.println("Success...");
    }
}
