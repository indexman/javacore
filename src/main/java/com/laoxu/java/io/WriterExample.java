package com.laoxu.java.io;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * @author laoxu
 * @create 2018-01-07
 **/
public class WriterExample {
    public static void main(String[] args) throws IOException {
        Writer w = new FileWriter("D:\\output.txt");
        String content = "I love my country.";
        w.write(content);
        w.close();
        System.out.println("Done.");
    }
}
