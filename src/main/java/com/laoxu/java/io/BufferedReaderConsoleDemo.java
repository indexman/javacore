package com.laoxu.java.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * 读取控制台输入流
 * @author laoxu
 * @create 2018-01-13
 **/
public class BufferedReaderConsoleDemo {
    public static void main(String[] args) throws IOException{
        Reader r = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(r);
        System.out.println("请输入您的姓名：");
        String name = br.readLine();
        if(!(name==null || "".equals(name))){
            System.out.println("欢迎"+name+"！");
        }

        br.close();
    }
}
