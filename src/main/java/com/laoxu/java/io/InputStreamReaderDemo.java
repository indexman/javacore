package com.laoxu.java.io;

import java.io.*;

/**
 * @author laoxu
 * @create 2018-01-13
 **/
public class InputStreamReaderDemo {
    public static void main(String[] args) throws IOException{
        InputStream r = new FileInputStream("D:\\testout.txt");
        InputStreamReader isr = new InputStreamReader(r,"GBK");
        BufferedReader br = new BufferedReader(isr);

        if (br.ready()) {
            String content;
            while ((content = br.readLine()) != null) {
                System.out.println(content);
            }
        }

        br.close();
    }
}
