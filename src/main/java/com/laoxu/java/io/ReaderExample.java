package com.laoxu.java.io;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

/**
 * @author laoxu
 * @create 2018-01-07
 **/
public class ReaderExample {
    public static void main(String[] args) throws IOException{
        Reader r = new FileReader("D:\\output.txt");
        int data = 0;
        while ((data=r.read())!=-1){
            System.out.print((char)data);
        }

        r.close();
    }
}
