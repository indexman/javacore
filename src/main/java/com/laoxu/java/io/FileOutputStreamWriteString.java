package com.laoxu.java.io;

import java.io.FileOutputStream;

/**
 * @author laoxu
 * @create 2017-12-31
 **/
public class FileOutputStreamWriteString {
    public static void main(String[] args) {
        try {
            FileOutputStream fout = new FileOutputStream("D:\\testout.txt",true);
            String s = "Welcome to java.io.";
            byte b[] = s.getBytes(); //将字符串转为字节数组
            fout.write(b);
            fout.close();
            System.out.println("写入成功！");
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }
}
