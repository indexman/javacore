package com.laoxu.java.io;

import java.io.FileInputStream;

/**
 * @author laoxu
 * @create 2017-12-31
 **/
public class FileInputStreamReadAllChars {
    public static void main(String[] args) {
        try {
            FileInputStream fin = new FileInputStream("d:\\testout.txt");
            int i=0;
            while ((i=fin.read())!=-1){
                System.out.print((char) i);
            }
            fin.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }
}
