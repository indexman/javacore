package com.laoxu.java.io;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author laoxu
 * @create 2018-01-06
 **/
public class DataInputStreamDemo {
    public static void main(String[] args) throws IOException{
        InputStream is = new FileInputStream("D:\\testout.txt");
        DataInputStream dis = new DataInputStream(is);
        int count = dis.available();
        byte[] bytes = new byte[count];
        dis.read(bytes);
        for(byte b:bytes){
            char c = (char)b;
            System.out.print(c);
        }
        dis.close();
    }
}
