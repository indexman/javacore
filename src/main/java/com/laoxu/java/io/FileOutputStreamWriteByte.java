package com.laoxu.java.io;

import java.io.FileOutputStream;

/**
 * @author laoxu
 * @create 2017-12-31
 **/
public class FileOutputStreamWriteByte {
    public static void main(String[] args) {
        try {
            FileOutputStream fout = new FileOutputStream("D:\\testout.txt");
            fout.write(65);
            fout.close();
            System.out.println("success...");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
