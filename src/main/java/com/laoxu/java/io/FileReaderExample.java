package com.laoxu.java.io;

import java.io.FileReader;

/**
 * @author laoxu
 * @create 2018-01-07
 **/
public class FileReaderExample {
    public static void main(String args[])throws Exception{
        FileReader fr=new FileReader("D:\\testout.txt");
        int i;
        while((i=fr.read())!=-1)
            System.out.print((char)i);
        fr.close();
    }
}
