package com.laoxu.java.io;

import com.laoxu.java.entity.Employee;

import java.io.*;

/**
 * @Description:
 * @Author laoxu
 * @Date 2020/10/31 10:09
 **/
public class ObjectOutPutStreamDemo {
    public static void main(String[] args) {
        try {
            String filepath = "d:/employee.dat";
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filepath));
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(filepath));
            Employee e = new Employee("Jack", 9000, "1990-03-09");
            out.writeObject(e);
            System.out.println("保存employee成功");
            Employee e1 = (Employee) in.readObject();
            System.out.println("读取employee: ");
            System.out.println(e1);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
