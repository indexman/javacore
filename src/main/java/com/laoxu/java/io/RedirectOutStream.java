package com.laoxu.java.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

/**
 * 重定向输出到文件
 *
 * @author laoxu
 * @create 2018-02-24
 **/
public class RedirectOutStream {
    public static void main(String[] args) {
        PrintStream out = System.out;
        try {
            File logFile = new File("E:\\java\\output\\out.log");
            if(!logFile.exists()){
                logFile.createNewFile();
            }
            PrintStream ps = new PrintStream(logFile);
            System.out.println("log being");
            System.setOut(ps); //切换标准输出流到日志文件
            System.out.println("Hello World!");
            System.out.println("This message come from standard output stream! ");
            System.setOut(out); //切换回来
            System.out.println("log end!");
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }
}
