package com.laoxu.java.io;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;

/**
 * @author laoxu
 * @create 2018-01-01
 **/
public class BufferedOutputStreamExample {
    public static void main(String[] args) {
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream("D:\\testout.txt");
            BufferedOutputStream bout = new BufferedOutputStream(fout);
            String s = "北京欢迎你！";
            byte b[] = s.getBytes();
            bout.write(b);
            bout.flush();
            bout.close();
            fout.close();
            System.out.println("写入成功！");
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

    }
}
