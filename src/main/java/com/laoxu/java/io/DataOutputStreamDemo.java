package com.laoxu.java.io;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author laoxu
 * @create 2018-01-06
 **/
public class DataOutputStreamDemo {
    public static void main(String[] args) throws IOException{
        OutputStream fos = new FileOutputStream("D:\\testout.txt");
        DataOutputStream dos = new DataOutputStream(fos);
        dos.writeInt(65);
        dos.writeUTF("Java is great!");
        dos.flush();
        dos.close();

        System.out.println("写入成功！");
    }
}
