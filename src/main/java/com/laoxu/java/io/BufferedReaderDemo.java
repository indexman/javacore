package com.laoxu.java.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

/**
 * @author laoxu
 * @create 2018-01-13
 **/
public class BufferedReaderDemo {
    public static void main(String[] args) throws IOException {
        Reader r = new FileReader("D:\\testout.txt");
        BufferedReader br = new BufferedReader(r);
        if (br.ready()) {
            String content = null;
            while ((content = br.readLine()) != null) {
                System.out.println(content);
            }
        }

        br.close();
    }
}
