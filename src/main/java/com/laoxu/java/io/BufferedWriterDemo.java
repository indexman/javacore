package com.laoxu.java.io;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * @author laoxu
 * @create 2018-01-13
 **/
public class BufferedWriterDemo {
    public static void main(String[] args) throws IOException{
        Writer w = new FileWriter("D:\\testout.txt");
        BufferedWriter bw = new BufferedWriter(w);
        bw.write("我是中国人");
        bw.newLine();
        bw.write("我此生无悔入华夏！");
        bw.flush();
        bw.close();
        System.out.println("写入成功！");
    }
}
