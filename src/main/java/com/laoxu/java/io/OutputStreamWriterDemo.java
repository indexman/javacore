package com.laoxu.java.io;

import java.io.*;

/**
 * @author laoxu
 * @create 2018-01-13
 **/
public class OutputStreamWriterDemo {
    public static void main(String[] args) throws IOException{
        OutputStream w = new FileOutputStream("D:\\testout.txt");
        OutputStreamWriter osw = new OutputStreamWriter(w,"GBK");
        BufferedWriter bw = new BufferedWriter(osw);
        bw.write("欢迎学习java.io");
        bw.flush();
        bw.close();
        System.out.println("写入完成！");
    }
}
