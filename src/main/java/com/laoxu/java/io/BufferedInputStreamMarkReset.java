package com.laoxu.java.io;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * @author laoxu
 * @create 2018-01-01
 **/
public class BufferedInputStreamMarkReset {
    public static void main(String[] args) throws Exception {

        InputStream iStream = null;
        BufferedInputStream bis = null;

        try {

            // read from file c:/test.txt to input stream
            iStream = new FileInputStream("D:/testmark.txt");

            // input stream converted to buffered input stream
            bis = new BufferedInputStream(iStream);

            // read and print characters one by one
            System.out.println("Char : " + (char) bis.read());
            System.out.println("Char : " + (char) bis.read());
            System.out.println("Char : " + (char) bis.read());

            // mark is set on the input stream
            bis.mark(0);
            System.out.println("Char : " + (char) bis.read());
            System.out.println("reset() invoked");

            // reset is called
            bis.reset();

            // read and print characters
            System.out.println("char : " + (char) bis.read());
            System.out.println("char : " + (char) bis.read());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            // releases any system resources associated with the stream
            if (iStream != null)
                iStream.close();
            if (bis != null)
                bis.close();
        }
    }
}
