package com.laoxu.java.test;

import java.io.Console;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * @author laoxu
 * @create 2017-12-22
 **/
public class Test {
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        System.out.println(calendar.get(Calendar.YEAR));
    }

    private void sayHello(String firstName, String lastName){
        System.out.println("your first name: "+firstName);
    }

    private void writeData(PrintWriter out) throws Exception{
        GregorianCalendar calendar = new GregorianCalendar();
        out.println();
    }
}
