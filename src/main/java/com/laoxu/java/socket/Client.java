package com.laoxu.java.socket;

import java.io.*;
import java.net.Socket;

/**
 * 客户端
 *
 * @author laoxu
 * @create 2017-12-20
 **/
public class Client {
    public static final String HOST = "127.0.0.1";//服务器地址
    public static final int PORT = 8858;//服务器端口号

    public static void main(String[] args) throws Exception{
        Socket client = new Socket(HOST,PORT);
        //创建输入流
        BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
        //创建输出流
        PrintWriter out = new PrintWriter(client.getOutputStream(),true);
        //向服务器程序发送信息
        out.println("Hello Server! This is Client! ");
        //接收来自服务器程序的响应
        String readLine=in.readLine();
        if(readLine!=null){
            System.out.println("Receive message from Server: "+readLine);
        }

        //关闭链接
        out.close();
        in.close();
        client.close();
    }
}
