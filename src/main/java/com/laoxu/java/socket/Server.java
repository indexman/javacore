package com.laoxu.java.socket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 带回声的服务器
 *
 * @author laoxu
 * @create 2017-12-21
 **/
public class Server {
    public static final int PORT = 8858;//监听的端口号

    public static void main(String[] args) throws Exception {
        System.out.println("Server started! Listening...");
        ServerSocket serverSocket = new ServerSocket(PORT);
        Socket server = serverSocket.accept();

        BufferedReader in = new BufferedReader(new InputStreamReader(server.getInputStream()));
        PrintWriter out = new PrintWriter(server.getOutputStream());

        String readLine = in.readLine();
        if (readLine != null) {
            System.out.println("Receive message from Client: " + readLine);
            out.println("Roger!");
        }

        out.close();
        in.close();
        server.close();
        serverSocket.close();


    }


}
