package com.laoxu.java.socket;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * @author laoxu
 * @create 2017-12-24
 **/
public class KKMultiServer {
    private static final int PORT = 8858;

    public static void main(String[] args) {
        boolean listening = true;

        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            while (listening) {
                new KKMultiServerThread(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            System.err.println("Could not listen on port " + PORT);
            System.exit(-1);
        }
    }
}
