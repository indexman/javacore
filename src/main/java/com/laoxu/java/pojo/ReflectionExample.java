package com.laoxu.java.pojo;

import org.apache.commons.beanutils.PropertyUtils;

import java.beans.PropertyDescriptor;

/**
 * @Description: 反射POJO和JavaBean
 * @Author laoxu
 * @Date 2021/6/12 23:28
 **/
public class ReflectionExample {
    public static void main(String[] args) {

        System.out.println("Fields for EmployeePojo are:");
        for (PropertyDescriptor pd : PropertyUtils.getPropertyDescriptors(EmployeePojo.class)) {
            System.out.println(pd.getDisplayName());
        }

        System.out.println("Fields for EmployeeBean are:");
        for (PropertyDescriptor pd : PropertyUtils.getPropertyDescriptors(EmployeeBean.class)) {
            System.out.println(pd.getDisplayName());
        }

    }
}
