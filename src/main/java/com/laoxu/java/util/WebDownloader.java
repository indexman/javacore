package com.laoxu.java.util;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * @Author laoxu
 * @Date 2022/11/22 14:45
 * @Desc 下载工具
 */
public class WebDownloader {
    public static void download(String url,String filename){
        try {
            FileUtils.copyURLToFile(new URL(url),new File(filename));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IO异常，下载器出现问题！");
        }
    }
}
