package com.laoxu.java.util;

/**
 * 基本的小工具
 *
 * @author laoxu
 * @create 2018-03-08
 **/
public class BaseUtils {
    public static void printArray(int[][] arr){
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j]+"\t");
            }
            System.out.println();
        }
    }
}
