package com.laoxu.java.datatype;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author laoxu
 * @create 2018-01-24
 **/
public class SimpleDateFormatTest {
    public static void main(String[] args) {
        //create date format
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/M/d");

        //convert date to specific format string
        Date date = new Date();
        System.out.println(sdf1.format(date));

        //convert string to date
        String dateStr="2018/1/24";
        try {
            System.out.println(sdf2.parse(dateStr));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
