package com.laoxu.java.net;

import java.io.IOException;

/**
 * @author laoxu
 * @create 2017-12-24
 **/
public class QuoteServer {
    public static void main(String[] args) throws IOException {
        new QuoteServerThread().start();
    }
}
