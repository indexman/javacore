package com.laoxu.java.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * 直接读取URL数据
 *
 * @author laoxu
 * @create 2017-12-23
 **/
public class URLReader {
    public static void main(String[] args) {
        try {
            URL url = new URL("http://www.baidu.com");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                System.out.println(inputLine);
            }

            in.close();

        } catch (MalformedURLException e) {
            System.out.println("URL格式不正确！");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
