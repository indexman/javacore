package com.laoxu.java.net;

/**
 * @author laoxu
 * @create 2017-12-25
 **/
public class MulticastServer {
    public static void main(String[] args) throws java.io.IOException {
        new MulticastServerThread().start();
    }
}
