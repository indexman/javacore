package com.laoxu.java.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * @Description: 员工实体
 * @Author laoxu
 * @Date 2020/10/30 14:28
 **/
@Data
@AllArgsConstructor
@ToString
public class Employee implements Serializable {
    private String name;
    private Integer salary;
    private String hireDate; // 入职日期

    public void raiseSalary(int inc){
        this.salary += inc;
    }
}
