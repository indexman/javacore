package com.laoxu.java.entity;

import lombok.Data;

/**
 * @Description: 经理
 * @Author laoxu
 * @Date 2020/10/31 10:31
 **/
@Data
public class Manager extends Employee {
    private Employee secretary; // 秘书

    public Manager(String name, Integer salary, String hireDate) {
        super(name, salary, hireDate);
    }

    @Override
    public String toString() {
        return "Manager{" + this.getName()+","+this.getSalary()+","+this.getHireDate()+","+
                "secretary=" + secretary +
                "} ";
    }
}
