package com.laoxu.java.exceptions.exceptionhandling;

public class TimeoutException extends Exception {

    public TimeoutException(String message) {
        super(message);
    }
}
