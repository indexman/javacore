package com.laoxu.java.designpattern;

/**
 * @Author laoxu
 * @Date 2022/11/22 21:26
 * @Desc 代理模式-静态代理
 *
 */
public class StaticProxy {
    public static void main(String[] args) {
        WeddingCompany wc = new WeddingCompany(new You());
        wc.goMarry();
    }
}

interface Marry{
    void goMarry();
}

class You implements Marry{
    @Override
    public void goMarry() {
        System.out.println("杰克要结婚了！");
    }
}

class WeddingCompany implements Marry{
    private Marry client;

    public WeddingCompany(Marry client){
        this.client = client;
    }
    @Override
    public void goMarry() {
        beforMarry();
        client.goMarry();
        afterMarry();
    }

    private void afterMarry() {
        System.out.println("婚礼结束收钱了！");
    }

    private void beforMarry() {
        System.out.println("婚礼布置...");
    }
}